"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const lodash_1 = __importDefault(require("lodash"));
function match(data = {}, filter = {}) {
    if (Array.isArray(filter)) {
        return matchAny(data, filter);
    }
    else {
        return matchAll(data, filter);
    }
}
exports.match = match;
function matchAny(data = {}, filter = []) {
    let match = false;
    for (const ft of filter) {
        if (matchAll(data, ft)) {
            match = true;
        }
    }
    return match;
}
exports.matchAny = matchAny;
function matchAll(data = {}, filter = {}) {
    let match = true;
    for (const [key, type] of Object.entries(filter)) {
        const target = lodash_1.default.get(data, key);
        if (typeof target === 'undefined') {
            return false;
        }
        if (typeof type === 'object') {
            for (const [op, value] of Object.entries(type)) {
                switch (op) {
                    case 'eq':
                        if (target !== value)
                            match = false;
                        break;
                    case 'neq':
                        if (target === value)
                            match = false;
                        break;
                    case 'gt':
                        if (typeof target !== 'number' || !(target > value))
                            match = false;
                        break;
                    case 'gte':
                        if (typeof target !== 'number' || !(target >= value))
                            match = false;
                        break;
                    case 'lt':
                        if (typeof target !== 'number' || !(target < value))
                            match = false;
                        break;
                    case 'lte':
                        if (typeof target !== 'number' || !(target <= value))
                            match = false;
                        break;
                    case 'inq':
                        if (!value.includes(target))
                            match = false;
                        break;
                    case 'nin':
                        if (value.includes(target))
                            match = false;
                        break;
                    case 'like':
                        if (typeof target !== 'string' || !target.includes(value))
                            match = false;
                        break;
                    case 'between':
                        if (typeof target !== 'number' || target < value[0] || value > value[1])
                            match = false;
                    default:
                        break;
                }
            }
        }
        else {
            if (target !== type)
                match = false;
        }
    }
    return match;
}
exports.matchAll = matchAll;
