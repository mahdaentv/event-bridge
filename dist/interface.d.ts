import { QueryFilter } from './filter';
export declare type MessageBody = string | number | JSONBody | any[];
export declare type JSONBody = {
    [key: string]: any;
};
export declare type ClientMessage<T> = {
    type: 'request' | 'subscription';
    data: ClientRequest<T> | ClientSubscription;
    uuid: string;
};
export declare type ClientRequest<T> = {
    method: RequestMethod;
    url: string;
    headers?: RequestHeaders;
    data?: T;
};
export declare type ClientSubscription = {
    method: 'subscribe' | 'unsubscribe';
    url: string;
    headers?: RequestHeaders;
    filters?: QueryFilter;
};
export declare type RequestMethod = 'post' | 'get' | 'put' | 'patch' | 'delete' | 'options' | 'subscribe' | 'unsubscribe';
export declare type RequestHeaders = {
    [key: string]: string;
};
export declare type ServerMessage<T> = {
    type: 'response' | 'event';
    data: ServerResponse<T> | ServerEvent<T>;
    uuid: string;
};
export declare type ServerResponse<T> = {
    status: number;
    statusText: string;
    headers?: RequestHeaders;
    data?: T;
};
export declare type ServerEventType = RequestMethod | 'touch';
export declare type ServerEvent<T> = {
    type: ServerEventType;
    path: string;
    filters?: QueryFilter;
    data?: T;
    referer?: ServerEvent<any>;
};
