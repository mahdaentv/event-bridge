/// <reference types="node" />
import { IncomingMessage, Server as HttpServer } from 'http';
import { ClientMessage, ServerEvent } from '../interface';
import WebSocket from 'ws';
import { RequestHandler } from './router';
import { Response } from './response';
import { Request } from './request';
import { User } from './user';
import { LogLevel } from '../logger';
export declare type ConnectionQuery = {
    clientId: string;
    reconnect?: boolean;
};
export declare type RequestMiddleware = (req: Request<any>, res: Response<any>) => void | Promise<void>;
export declare type ServerConfig = {
    logLevel?: LogLevel;
    keepAlive?: number;
    publishChanges?: boolean;
};
export declare class Server {
    private config;
    private users;
    private subscriptions;
    private socket;
    private router;
    private middlewares;
    constructor(server: HttpServer, config?: ServerConfig);
    use(middleware: RequestMiddleware): this;
    all<R, D>(path: string, handler: RequestHandler<R, D>): this;
    post<R, D>(path: string, handler: RequestHandler<R, D>): this;
    get<D>(path: string, handler: RequestHandler<null, D>): this;
    put<R, D>(path: string, handler: RequestHandler<R, D>): this;
    patch<R, D>(path: string, handler: RequestHandler<R, D>): this;
    options(path: string, handler: RequestHandler<null, null>): this;
    delete<D>(path: string, handler: RequestHandler<null, D>): this;
    emit<T>(event: ServerEvent<T>): void;
    protected connect(client: WebSocket, request: IncomingMessage): void;
    protected response<R, D>(user: User, message: ClientMessage<R>): Promise<void>;
    protected subscription(user: User, message: ClientMessage<any>): Promise<void>;
    protected cleanup(): void;
}
