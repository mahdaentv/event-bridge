"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const ws_1 = __importDefault(require("ws"));
const logger_1 = require("../logger");
class User {
    constructor(context) {
        this.queues = [];
        Object.assign(this, context);
        this.client.on('close', () => {
            this.disconnected = new Date();
            logger_1.Logger.info(`User disconnected: ${this.clientId}. Scheduled for cleanup.`);
        });
        logger_1.Logger.info(`User connected: ${this.clientId}.`);
    }
    send(uuid, data) {
        if (this.client.readyState === ws_1.default.OPEN) {
            this.client.send(JSON.stringify({ type: 'event', uuid, data }));
            logger_1.Logger.info(`Event sent to: ${uuid}.`);
        }
        else {
            this.queues.push({ uuid, data });
            logger_1.Logger.info(`Event queued: ${uuid}.`);
        }
    }
    response(uuid, data) {
        data.headers = this.createHeaders(data);
        this.client.send(JSON.stringify({ type: 'response', uuid, data }));
        logger_1.Logger.info(`Response sent: ${uuid}.`);
    }
    upgrade(context, reconnect) {
        Object.assign(this, context);
        this.disconnected = null;
        this.client.on('close', () => {
            this.disconnected = new Date();
            logger_1.Logger.info(`User disconnected: ${this.clientId}. Scheduled for cleanup.`);
        });
        if (reconnect && this.queues.length) {
            for (const queue of this.queues) {
                this.send(queue.uuid, queue.data);
            }
            this.queues = [];
        }
        logger_1.Logger.info(`User reconnected: ${this.clientId}.`);
    }
    createHeaders(data) {
        const headers = Object.assign(Object.assign({}, data.headers || {}), { 'X-Powered-By': 'Event Bridge', 'Date': new Date().toISOString() });
        if (typeof data.data === 'string') {
            headers['Content-Type'] = 'text/html';
            headers['Content-Length'] = data.data.length;
        }
        else if (typeof data === 'object') {
            headers['Content-Type'] = 'application/json';
        }
        else {
            headers['Content-Type'] = '*/*';
        }
        return headers;
    }
}
exports.User = User;
