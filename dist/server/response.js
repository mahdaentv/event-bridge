"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Response {
    constructor(user, request) {
        this.user = user;
        this.request = request;
        this.raw = {
            status: 200,
            statusText: 'Success',
            headers: {}
        };
        this.state = 'ready';
    }
    status(status) {
        this.raw.status = status;
        return this;
    }
    statusText(text) {
        this.raw.statusText = text;
        return this;
    }
    set(key, value) {
        this.raw.headers[key] = value;
        return this;
    }
    setHeader(key, value) {
        this.set(key, value);
        return this;
    }
    send(body) {
        const { uuid } = this.request;
        if (this.raw.status < 200 || this.raw.status >= 300) {
            this.raw.statusText = 'Error.';
        }
        this.user.response(uuid, Object.assign(Object.assign({}, this.raw), { data: body }));
        this.body = body;
        this.state = 'sent';
    }
}
exports.Response = Response;
