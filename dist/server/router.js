"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const querystring_1 = require("querystring");
const url_pattern_1 = __importDefault(require("url-pattern"));
const logger_1 = require("../logger");
class Router {
    constructor() {
        this._get = [];
        this._post = [];
        this._put = [];
        this._patch = [];
        this._delete = [];
        this._options = [];
    }
    add(method, path, handler) {
        const routes = this[`_${method}`];
        if (routes) {
            routes.push(new Route(method, path, handler));
        }
        else {
            throw new Error('Invalid method.');
        }
        return this;
    }
    get(method, path) {
        const routes = this[`_${method}`];
        if (routes) {
            const targets = routes.filter(route => route.path === path);
            if (targets.length) {
                return targets[1];
            }
        }
        else {
            throw new Error('Invalid method.');
        }
    }
    rem(method, path) {
        const routes = this[`_${method}`];
        if (routes) {
            routes.forEach(route => {
                if (route.path === path) {
                    routes.splice(routes.indexOf(route), 1);
                }
            });
        }
        else {
            throw new Error('Invalid method.');
        }
        return this;
    }
    match(method, url) {
        const [path, query] = url.split('?');
        const routes = this[`_${method}`];
        if (routes) {
            const match = routes.filter(route => route.match(path));
            if (match.length) {
                return {
                    route: match[0],
                    params: match[0].match(path),
                    query: query ? querystring_1.parse(query) : {},
                };
            }
        }
        else {
            throw new Error('Invalid method.');
        }
    }
}
exports.Router = Router;
class Route {
    constructor(method, path, handler) {
        this.method = method;
        this.path = path;
        this.handler = handler;
        if (!path.startsWith('/')) {
            this.path = `/${path}`;
        }
        this.pattern = new url_pattern_1.default(this.path);
        logger_1.Logger.info(`Added route: [${method.toUpperCase()} ${this.path}].`);
    }
    match(url) {
        return this.pattern.match(url);
    }
}
exports.Route = Route;
