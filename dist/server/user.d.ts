import WebSocket from 'ws';
import { ServerResponse, ServerEvent } from '../interface';
import { QueryFilter } from '../filter';
export declare type Users = {
    [id: string]: User;
};
export declare type UserContext = {
    client: WebSocket;
    clientId: string;
};
export declare type UserSubscription = {
    user: User;
    filters?: QueryFilter;
};
export declare class User {
    private queues;
    client: WebSocket;
    clientId: string;
    disconnected: Date;
    constructor(context: UserContext);
    send<D>(uuid: string, data: ServerEvent<D>): void;
    response<D>(uuid: string, data: ServerResponse<D>): void;
    upgrade(context: UserContext, reconnect?: boolean): void;
    protected createHeaders(data: ServerResponse<any>): {
        'X-Powered-By': string;
        Date: string;
    };
}
