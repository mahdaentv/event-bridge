import { QueryFilter } from '../filter';
import { User } from './user';
import { ServerEvent } from '../interface';
export declare type Subscribers = Subscription[];
export declare class Subscriptions {
    private paths;
    subscribe(path: string, user: User, filters?: QueryFilter): Subscription;
    unsubscribe(path: string, user: User, filters?: QueryFilter): void;
    rem(user: User): void;
    find(path: string, data?: any): Subscribers;
    emit<D>(event: ServerEvent<D>): void;
    protected send<D>(path: string, data: D, event: ServerEvent<D>, origin?: ServerEvent<D>): void;
}
declare class Subscription {
    user: User;
    path: string;
    filters: QueryFilter;
    id: string;
    constructor(user: User, path: string, filters?: QueryFilter);
    send<D>(data: ServerEvent<D>): void;
}
export {};
