"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Request {
    constructor(app, originalUrl, method, headers) {
        this.app = app;
        this.originalUrl = originalUrl;
        this.method = method;
        this.headers = headers;
        this.query = {};
        this.params = {};
        this.body = null;
    }
    get path() {
        return this.originalUrl.split('?')[0];
    }
}
exports.Request = Request;
