import { RequestHeaders, RequestMethod } from '../interface';
import { Route, RouteParams, RouteQuery } from './router';
import { Server } from './server';
export declare class Request<T> {
    app: Server;
    originalUrl: string;
    method: RequestMethod;
    headers: RequestHeaders;
    route: Route<any, any>;
    query: RouteQuery;
    params: RouteParams;
    body: T;
    get path(): string;
    constructor(app: Server, originalUrl: string, method: RequestMethod, headers: RequestHeaders);
}
