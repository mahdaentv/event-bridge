import { RequestMethod } from '../interface';
import { Request } from './request';
import { Response } from './response';
export declare type RouteParams = {
    [key: string]: string | number | boolean;
};
export declare type RouteQuery = {
    [key: string]: string | number | boolean | object | any[];
};
export declare type RequestRoute<R, D> = {
    route: Route<R, D>;
    params: RouteParams;
    query: RouteQuery;
};
export declare type RequestHandler<R, D> = (req: Request<R>, res: Response<D>) => void | Promise<void>;
export declare class Router {
    private _get;
    private _post;
    private _put;
    private _patch;
    private _delete;
    private _options;
    add<R, D>(method: RequestMethod, path: string, handler: RequestHandler<R, D>): this;
    get<R, D>(method: RequestMethod, path: string): Route<R, D>;
    rem(method: RequestMethod, path: string): this;
    match<R, D>(method: RequestMethod, url: string): RequestRoute<R, D>;
}
export declare class Route<R, D> {
    method: string;
    path: string;
    handler: RequestHandler<R, D>;
    private pattern;
    constructor(method: string, path: string, handler: RequestHandler<R, D>);
    match(url: string): any;
}
