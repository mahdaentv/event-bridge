"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const filter_1 = require("../filter");
const uuid_1 = require("uuid");
const logger_1 = require("../logger");
class Subscriptions {
    constructor() {
        this.paths = {};
    }
    subscribe(path, user, filters = {}) {
        if (!this.paths[path]) {
            this.paths[path] = [];
        }
        const existing = this.paths[path].filter(sub => {
            return sub.user === user && JSON.stringify(filters) === JSON.stringify(sub.filters);
        });
        if (existing.length) {
            return existing[0];
        }
        else {
            const sub = new Subscription(user, path, filters);
            this.paths[path].push(sub);
            return sub;
        }
    }
    unsubscribe(path, user, filters = {}) {
        if (!this.paths[path]) {
            return;
        }
        const subscribers = this.paths[path];
        subscribers.forEach(sub => {
            if (sub.user === user && sub.path === path && JSON.stringify(filters) === JSON.stringify(sub.filters)) {
                subscribers.splice(subscribers.indexOf(sub), 1);
            }
        });
        if (!subscribers.length) {
            delete this.paths[path];
        }
    }
    rem(user) {
        for (const [path, subs] of Object.entries(this.paths)) {
            for (const sub of subs) {
                if (sub.user === user) {
                    subs.splice(subs.indexOf(sub), 1);
                }
            }
            if (!subs.length) {
                delete this.paths[path];
            }
        }
    }
    find(path, data = {}) {
        if (!this.paths[path]) {
            return [];
        }
        return this.paths[path].filter(sub => {
            return sub.path === path && filter_1.match(typeof data === 'object' ? data : {}, sub.filters);
        });
    }
    emit(event) {
        const { type, path, data } = event;
        this.send(path, data, event);
        const paths = path.replace(/^\//, '').split('/');
        if (paths.length > 1) {
            this.send(`/${paths.pop()}`, data, event, event);
            if (paths.length) {
                let base = '';
                for (const p of paths) {
                    base = `${base}/${p}`;
                    this.send(base, data, event, event);
                }
            }
        }
    }
    send(path, data, event, origin) {
        const { type } = event;
        this.find(path, data).forEach(sub => {
            const payload = {
                type,
                path: sub.path,
                data,
                filters: sub.filters,
            };
            if (origin) {
                payload.referer = { type: origin.type, path: origin.path };
            }
            logger_1.Logger.info(`Event sent: [${event.type.toUpperCase()} ${path}].`);
            sub.send(payload);
        });
    }
}
exports.Subscriptions = Subscriptions;
class Subscription {
    constructor(user, path, filters = {}) {
        this.user = user;
        this.path = path;
        this.filters = filters;
        this.id = uuid_1.v4();
    }
    send(data) {
        this.user.send(this.id, data);
    }
}
