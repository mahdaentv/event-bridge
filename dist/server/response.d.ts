import { ClientMessage } from '../interface';
import { User } from './user';
export declare class Response<D> {
    private user;
    private request;
    private raw;
    state: 'ready' | 'sent';
    body: D;
    constructor(user: User, request: ClientMessage<any>);
    status(status: number): this;
    statusText(text: string): this;
    set(key: string, value: string): this;
    setHeader(key: string, value: string): this;
    send(body?: D): void;
}
