"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const ws_1 = __importDefault(require("ws"));
const querystring_1 = require("querystring");
const router_1 = require("./router");
const response_1 = require("./response");
const request_1 = require("./request");
const user_1 = require("./user");
const subscription_1 = require("./subscription");
const logger_1 = require("../logger");
class Server {
    constructor(server, config = {}) {
        this.config = config;
        this.users = {};
        this.subscriptions = new subscription_1.Subscriptions();
        this.router = new router_1.Router();
        this.middlewares = [];
        if (config.logLevel) {
            logger_1.Logger.config.level = config.logLevel;
        }
        this.socket = new ws_1.default.Server({ server });
        this.socket.on('connection', (client, request) => {
            this.connect(client, request);
        });
        this.cleanup();
        logger_1.Logger.info('Server created and ready.');
    }
    use(middleware) {
        this.middlewares.push(middleware);
        return this;
    }
    all(path, handler) {
        ['get', 'post', 'put', 'patch', 'delete', 'options'].forEach(method => {
            this.router.add(method, path, handler);
        });
        return this;
    }
    post(path, handler) {
        this.router.add('post', path, handler);
        return this;
    }
    get(path, handler) {
        this.router.add('get', path, handler);
        return this;
    }
    put(path, handler) {
        this.router.add('put', path, handler);
        return this;
    }
    patch(path, handler) {
        this.router.add('patch', path, handler);
        return this;
    }
    options(path, handler) {
        this.router.add('options', path, handler);
        return this;
    }
    delete(path, handler) {
        this.router.add('delete', path, handler);
        return this;
    }
    emit(event) {
        this.subscriptions.emit(event);
    }
    connect(client, request) {
        const params = querystring_1.parse(request.url.split('?')[1] || '');
        const { clientId, reconnect } = params;
        const context = { client, clientId };
        logger_1.Logger.debug(`Connecting user: ${clientId}...`);
        let user;
        if (this.users[clientId]) {
            user = this.users[clientId];
            user.upgrade(context, reconnect);
        }
        else {
            user = new user_1.User(context);
            this.users[clientId] = user;
        }
        user.client.on('message', async (data) => {
            const message = JSON.parse(data);
            message.data.method = message.data.method.toLowerCase();
            if (message.type === 'request') {
                await this.response(user, message);
            }
            else if (message.type === 'subscription') {
                await this.subscription(user, message);
            }
        });
    }
    async response(user, message) {
        const { uuid } = message;
        const { url, method, headers, data: body } = message.data;
        logger_1.Logger.debug(`Initializing request ${uuid} [${method.toUpperCase()} ${url}]...`);
        try {
            const req = new request_1.Request(this, url, method, headers);
            const res = new response_1.Response(user, message);
            const { route, params, query } = this.router.match(method, url) || {};
            if (route) {
                Object.assign(req, { route, params, query, body });
                try {
                    for (const middleware of this.middlewares) {
                        await middleware(req, res);
                    }
                    await route.handler(req, res);
                    if (this.config.publishChanges && ['post', 'put', 'patch', 'delete'].includes(req.method)) {
                        this.emit({
                            path: req.path,
                            type: method,
                            data: res.body
                        });
                    }
                    if (res.state !== 'sent') {
                        res
                            .status(200)
                            .statusText('Success')
                            .send();
                    }
                    logger_1.Logger.info(`Request complete: ${uuid}.`);
                }
                catch (error) {
                    logger_1.Logger.error(`${error.message} - ${uuid}.`, error);
                    if (res.state !== 'sent') {
                        res
                            .status(500)
                            .statusText(error.message)
                            .send();
                    }
                }
            }
            else {
                res
                    .status(404)
                    .statusText('Not Found.')
                    .send();
            }
        }
        catch (error) {
            logger_1.Logger.error(`${error.message} - ${uuid}.`, error);
            const data = {
                status: 500,
                statusText: error.message
            };
            user.response(uuid, data);
        }
    }
    async subscription(user, message) {
        const { uuid } = message;
        logger_1.Logger.debug(`Initializing subscription: ${uuid}.`);
        const { url, method, headers, filters } = message.data;
        try {
            const req = new request_1.Request(this, url, method, headers);
            const res = new response_1.Response(user, message);
            const { route, params, query } = this.router.match('get', url) || {};
            if (route) {
                Object.assign(req, { route, params, query });
                try {
                    for (const middleware of this.middlewares) {
                        await middleware(req, res);
                    }
                    if (method === 'subscribe') {
                        const sub = this.subscriptions.subscribe(req.path, user, filters);
                        res
                            .status(200)
                            .statusText('Subscribed.')
                            .send({ id: sub.id });
                        logger_1.Logger.info(`User subscribed: ${uuid}@${req.path}.`);
                    }
                    else if (method === 'unsubscribe') {
                        this.subscriptions.unsubscribe(req.path, user, filters);
                        res
                            .status(200)
                            .statusText('Unsubscribed.')
                            .send();
                        logger_1.Logger.info(`User unsubscribed: ${uuid}@${req.path}.`);
                    }
                    else {
                        res
                            .status(400)
                            .statusText('Bad request.')
                            .send();
                    }
                }
                catch (error) {
                    logger_1.Logger.error(`${error.message} - ${uuid}.`, error);
                    if (res.state !== 'sent') {
                        res
                            .status(500)
                            .statusText(error.message)
                            .send();
                    }
                }
            }
            else {
                res
                    .status(404)
                    .statusText('Not found.')
                    .send();
            }
        }
        catch (error) {
            logger_1.Logger.error(`${error.message} - ${uuid}.`, error);
            const data = {
                status: 500,
                statusText: error.message
            };
            user.response(uuid, data);
        }
    }
    cleanup() {
        setTimeout(() => {
            for (const [id, user] of Object.entries(this.users)) {
                if (user.disconnected) {
                    const exp = user.disconnected.getTime() + (this.config.keepAlive || 6000);
                    const now = new Date().getTime();
                    if (exp <= now) {
                        delete this.users[id];
                        this.subscriptions.rem(user);
                        logger_1.Logger.debug(`Cleaned up user: ${id}.`);
                    }
                }
            }
            this.cleanup();
        }, 500);
    }
}
exports.Server = Server;
