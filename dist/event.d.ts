export declare type EventHandler<T> = (...events: T[]) => void;
export declare type Unsubscriber = () => void;
export declare class EventEmitter {
    listeners: EventHandler<any>[];
    emit<T>(...events: T[]): void;
    subscribe<T>(handler: EventHandler<T>): Unsubscriber;
    unsubscribe<T>(handler: EventHandler<T>): void;
}
