export declare function match(data?: object, filter?: QueryFilter): boolean;
export declare function matchAny(data?: object, filter?: WhereFilter[]): boolean;
export declare function matchAll(data?: object, filter?: WhereFilter): boolean;
export declare type QueryFilter = WhereFilter | WhereFilter[];
export declare type WhereFilter = {
    [key: string]: ValueType | WhereCondition;
};
export declare type WhereCondition = {
    eq?: any;
    neq?: any;
    gt?: number;
    gte?: number;
    lt?: number;
    lte?: number;
    inq?: Array<ValueType>;
    nin?: Array<ValueType>;
    like?: string;
    between?: [number, number];
};
export declare type ValueType = string | number | null | Date;
