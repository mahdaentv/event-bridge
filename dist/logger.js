"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const event_1 = require("./event");
const isBrowser = 'global' in this;
var LogLevel;
(function (LogLevel) {
    LogLevel[LogLevel["DEBUG"] = 0] = "DEBUG";
    LogLevel[LogLevel["INFO"] = 1] = "INFO";
    LogLevel[LogLevel["WARN"] = 2] = "WARN";
    LogLevel[LogLevel["ERROR"] = 3] = "ERROR";
})(LogLevel = exports.LogLevel || (exports.LogLevel = {}));
class Logger {
    static debug(message, data) {
        this.ondebug.emit(message, data);
        if (this.config.level <= LogLevel.DEBUG) {
            print(message, 'debug');
        }
    }
    static info(message, data) {
        this.oninfo.emit(message, data);
        if (this.config.level <= LogLevel.INFO) {
            print(message, 'info');
        }
    }
    static warn(message, data) {
        this.onwarn.emit(message, data);
        if (this.config.level <= LogLevel.WARN) {
            print(message, 'warn');
        }
    }
    static error(message, error) {
        this.onerror.emit(message, error);
        if (this.config.level <= LogLevel.ERROR) {
            print(message, 'error');
        }
    }
}
exports.Logger = Logger;
Logger.config = {
    level: LogLevel.DEBUG
};
Logger.ondebug = new event_1.EventEmitter();
Logger.oninfo = new event_1.EventEmitter();
Logger.onwarn = new event_1.EventEmitter();
Logger.onerror = new event_1.EventEmitter();
const printref = {
    debug: {
        icon: '[ ]',
        color: '\x1b[37m%s\x1b[0m',
        fn: console.log
    },
    info: {
        icon: '[✓︎]',
        color: '\x1b[36m%s\x1b[0m',
        fn: console.info
    },
    warn: {
        icon: '[△]',
        color: '\x1b[33m%s\x1b[0m',
        fn: console.warn
    },
    error: {
        icon: '[✕]',
        color: '\x1b[31m%s\x1b[0m',
        fn: console.error
    },
};
function print(message, level) {
    printref[level].fn(printref[level].color, `${printref[level].icon} ${message}`);
}
