"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const lodash_1 = __importDefault(require("lodash"));
const uuid_1 = require("uuid");
const event_1 = require("../event");
const logger_1 = require("../logger");
class Client {
    constructor(config) {
        this.config = config;
        this.requests = {};
        this.subscriptions = {};
        this.queues = [];
        this.retries = 0;
        this.status = 'init';
        this.connected = new event_1.EventEmitter();
        this.disconnected = new event_1.EventEmitter();
        this.message = new event_1.EventEmitter();
        if (config.logLevel) {
            logger_1.Logger.config.level = config.logLevel;
        }
        if (config.autoconnect) {
            this.connect();
        }
    }
    get baseURL() {
        return this.config.baseURL.replace(/^http/, 'ws');
    }
    get readyState() {
        return this.client.readyState;
    }
    connect(reconnect) {
        let url = `${this.baseURL}?clientId=${this.config.clientId || uuid_1.v4()}`;
        if (reconnect) {
            url = `${url}&reconnect=true`;
        }
        this.client = new WebSocket(url);
        this.client.onopen = (e) => {
            this.status = 'ready';
            this.retries = 0;
            this.connected.emit(e);
            if (this.queues.length) {
                for (const queue of this.queues) {
                    queue.resolve();
                    this.queues.splice(this.queues.indexOf(queue), 1);
                }
            }
        };
        this.client.onclose = (e) => {
            this.disconnected.emit(e);
            if (this.retries >= 20) {
                const error = new Error('WebSocket connection retries reach the limit. Connection failed!');
                logger_1.Logger.error(error.message, error);
                if (this.queues.length) {
                    for (const queue of this.queues) {
                        queue.reject();
                        this.queues.splice(this.queues.indexOf(queue), 1);
                    }
                }
            }
            if (this.status === 'init') {
                this.retries += 1;
                this.connect();
            }
            else {
                if (this.config.keepAlive) {
                    this.connect(true);
                }
            }
        };
        this.client.onmessage = (msg) => {
            this.message.emit(msg);
            const message = JSON.parse(msg.data);
            if (message.type === 'response') {
                const data = message.data;
                if (this.requests[message.uuid]) {
                    this.requests[message.uuid](data);
                }
            }
            else if (message.type === 'event') {
                const data = message.data;
                if (this.subscriptions[message.uuid]) {
                    this.subscriptions[message.uuid].forEach(sub => sub.handler(data));
                }
            }
        };
    }
    async get(url, options) {
        return await this.request(Object.assign({ url, method: 'get' }, options));
    }
    async post(url, data, options) {
        return await this.request(Object.assign({ url, method: 'post', data }, options));
    }
    async put(url, data, options) {
        return await this.request(Object.assign({ url, method: 'put', data }, options));
    }
    async delete(url, options) {
        return await this.request(Object.assign({ url, method: 'delete' }, options));
    }
    async options(url, options) {
        return await this.request(Object.assign({ url, method: 'options' }, options));
    }
    async request(config) {
        const data = this.createRequest(config);
        return await this.submit({ type: 'request', uuid: uuid_1.v4(), data });
    }
    async subscribe(path, handler, options = {}) {
        if (!path) {
            throw new Error('Subscription path is required.');
        }
        if (!handler) {
            throw new Error('Subscription handler is required.');
        }
        const { params = {}, headers = {} } = options;
        const filters = options.filters || lodash_1.default.get(params, 'filter.where') || {};
        const req = this.createRequest({ url: path, method: 'subscribe', params, headers });
        const request = Object.assign(Object.assign({}, req), { filters });
        try {
            const response = await this.submit({ type: 'subscription', uuid: uuid_1.v4(), data: request });
            const subscription = new Subscription(request, response, handler);
            const info = {
                handler,
                subscription
            };
            if (!this.subscriptions[subscription.id]) {
                this.subscriptions[subscription.id] = [];
            }
            this.subscriptions[subscription.id].push(info);
            subscription.unsubscribe = async () => {
                await this.unsubscribe(path, options);
                this.subscriptions[subscription.id].splice(this.subscriptions[subscription.id].indexOf(info), 1);
            };
            return subscription;
        }
        catch (error) {
            throw error;
        }
    }
    async unsubscribe(path, options) {
        const { params = {}, headers = {} } = options;
        const filters = options.filters || lodash_1.default.get(params, 'filter.where') || {};
        const req = this.createRequest({ url: path, method: 'unsubscribe', params, headers });
        const request = Object.assign(Object.assign({}, req), { filters });
        try {
            await this.submit({ type: 'subscription', uuid: uuid_1.v4(), data: request });
        }
        catch (error) {
            throw error;
        }
    }
    createRequest(config) {
        const { url, method, params = {}, headers = {}, data } = config;
        return {
            method, data,
            headers: Object.assign(Object.assign({}, this.config.headers || {}), headers),
            url: this.createURL(url, params),
        };
    }
    createURL(url, params = {}) {
        if (!url.startsWith('/')) {
            url = `/${url}`;
        }
        return params && Object.keys(params).length ? `${url}?${stringify(params)}` : url;
    }
    async submit(message, options = {}) {
        const start = new Date().getTime();
        if (this.status !== 'ready') {
            await new Promise((resolve, reject) => {
                this.queues.push({ resolve, reject });
            });
        }
        return await new Promise((resolve, reject) => {
            this.requests[message.uuid] = (res) => {
                if (res.status >= 200 && res.status < 300) {
                    resolve(res);
                }
                else {
                    reject(new RequestError(message.data, res));
                }
                delete this.requests[message.uuid];
                logger_1.Logger.info(`Request ${message.uuid} finished in: ${new Date().getTime() - start}ms.`);
            };
            if (options.timeout || this.config.timeout) {
                setTimeout(() => {
                    reject(new RequestTimeout(message.data));
                }, options.timeout || this.config.timeout);
            }
            this.client.send(JSON.stringify(message));
        });
    }
}
exports.Client = Client;
class Subscription {
    constructor(request, response, handler) {
        this.request = request;
        this.response = response;
        this.handler = handler;
        if (response.data && response.data.id) {
            this.id = response.data.id;
        }
        else {
            throw new Error('Missing subscription id.');
        }
    }
    emit(event) {
        this.handler(event);
    }
}
exports.Subscription = Subscription;
class RequestTimeout extends Error {
    constructor(request, message = 'Request timed out.') {
        super(message);
        this.request = request;
        this.code = 408;
    }
}
exports.RequestTimeout = RequestTimeout;
class RequestError extends Error {
    constructor(request, response) {
        super(response.statusText);
        this.request = request;
        this.response = response;
        this.code = response.status;
    }
}
exports.RequestError = RequestError;
function stringify(params) {
    const query = new URLSearchParams();
    for (const [key, value] of Object.entries(params)) {
        if (value) {
            if (Array.isArray(value)) {
                for (const val of value) {
                    query.append(key, val);
                }
            }
            else if (toString.call(value) === '[object Object]') {
                query.set(key, JSON.stringify(value));
            }
            else if (toString.call(value) === '[object Date]') {
                query.set(key, value.toISOString());
            }
            else {
                query.set(key, value);
            }
        }
    }
    return query.toString();
}
exports.stringify = stringify;
