import { EventEmitter } from '../event';
import { QueryFilter } from '../filter';
import { ClientMessage, ClientRequest, ClientSubscription, RequestHeaders, RequestMethod, ServerEvent, ServerResponse } from '../interface';
import { LogLevel } from '../logger';
export interface ClientConfig {
    baseURL: string;
    clientId?: string;
    headers?: RequestHeaders;
    timeout?: number;
    logLevel?: LogLevel;
    autoconnect?: boolean;
    keepAlive?: boolean;
}
export declare type QueryParams = {
    [key: string]: string | boolean | number | Date | string[] | number[] | object;
};
export interface RequestOptions {
    params?: QueryParams;
    filters?: QueryFilter;
    headers?: RequestHeaders;
    timeout?: number;
}
export interface RequestConfig<T> extends RequestOptions {
    url?: string;
    method?: RequestMethod;
    data?: T;
}
export declare type Requests = {
    [id: string]: (res: ServerResponse<any>) => void;
};
export declare type SubscriptionHandler<D> = (event: ServerEvent<D>) => void | Promise<void>;
export declare class Client {
    protected config: ClientConfig;
    private client;
    private requests;
    private subscriptions;
    private queues;
    private retries;
    status: 'init' | 'ready';
    connected: EventEmitter;
    disconnected: EventEmitter;
    message: EventEmitter;
    private get baseURL();
    get readyState(): number;
    constructor(config: ClientConfig);
    connect(reconnect?: boolean): void;
    get<D>(url: string, options?: RequestOptions): Promise<ServerResponse<D>>;
    post<R, D>(url: string, data: R, options?: RequestOptions): Promise<ServerResponse<D>>;
    put<R, D>(url: string, data: R, options?: RequestOptions): Promise<ServerResponse<D>>;
    delete<D>(url: string, options?: RequestOptions): Promise<ServerResponse<D>>;
    options(url: string, options?: RequestOptions): Promise<ServerResponse<void>>;
    request<R, D>(config: RequestConfig<R>): Promise<ServerResponse<D>>;
    subscribe<D>(path: string, handler: SubscriptionHandler<D>, options?: RequestOptions): Promise<Subscription<D>>;
    protected unsubscribe(path: string, options?: RequestOptions): Promise<void>;
    protected createRequest<R>(config: RequestConfig<R>): ClientRequest<R>;
    protected createURL(url: string, params?: QueryParams): string;
    protected submit<R, D>(message: ClientMessage<R>, options?: RequestOptions): Promise<ServerResponse<D>>;
}
export declare class Subscription<D> {
    request: ClientSubscription;
    response: ServerResponse<any>;
    private handler;
    id: string;
    unsubscribe: () => void;
    constructor(request: ClientSubscription, response: ServerResponse<any>, handler: SubscriptionHandler<D>);
    emit(event: ServerEvent<D>): void;
}
export declare class RequestTimeout extends Error {
    request: ClientRequest<any>;
    code: number;
    constructor(request: ClientRequest<any>, message?: string);
}
export declare class RequestError extends Error {
    request: ClientRequest<any>;
    response: ServerResponse<any>;
    code: number;
    constructor(request: ClientRequest<any>, response: ServerResponse<any>);
}
export declare function stringify(params: QueryParams): string;
