import { EventEmitter } from './event';
export declare enum LogLevel {
    DEBUG = 0,
    INFO = 1,
    WARN = 2,
    ERROR = 3
}
export declare type LoggerConfig = {
    level: LogLevel;
};
export declare class Logger {
    static config: LoggerConfig;
    static ondebug: EventEmitter;
    static oninfo: EventEmitter;
    static onwarn: EventEmitter;
    static onerror: EventEmitter;
    static debug(message: string, data?: any): void;
    static info(message: string, data?: any): void;
    static warn(message: string, data?: any): void;
    static error(message: string, error: Error): void;
}
