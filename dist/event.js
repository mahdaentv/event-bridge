"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class EventEmitter {
    constructor() {
        this.listeners = [];
    }
    emit(...events) {
        for (const handler of this.listeners) {
            if (typeof handler === 'function') {
                handler(...events);
            }
        }
    }
    subscribe(handler) {
        if (Array.isArray(this.listeners)) {
            if (typeof handler === 'function' && !this.listeners.includes(handler)) {
                this.listeners.push(handler);
                return () => this.unsubscribe(handler);
            }
        }
    }
    unsubscribe(handler) {
        if (Array.isArray(this.listeners)) {
            this.listeners.splice(this.listeners.indexOf(handler), 1);
        }
    }
}
exports.EventEmitter = EventEmitter;
