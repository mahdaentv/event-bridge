"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const cors_1 = __importDefault(require("cors"));
const express_1 = __importDefault(require("express"));
const http_1 = require("http");
const uuid_1 = require("uuid");
const logger_1 = require("./logger");
const server_1 = require("./server/server");
const { DRIVER_PORT, DRIVER_SECRET, SERVER_URL } = process.env;
const app = express_1.default();
const svr = http_1.createServer(app);
const api = new server_1.Server(svr, { logLevel: logger_1.LogLevel.INFO });
app.use(express_1.default.json());
api.all('/**', (req, res) => {
    res.send(req.body || { id: uuid_1.v4(), meta: {}, data: [] });
});
app.all('/**', cors_1.default({
    origin: (origin, cb) => {
        cb(null, origin);
    }
}), (req, res) => {
    if (req.headers['x-server-secret'] !== DRIVER_SECRET) {
        res.status(401);
        res.send('Unauthorized access.');
        return;
    }
    if (['post', 'put', 'patch', 'delete'].includes(req.method.toLowerCase())) {
        api.emit({ type: req.method.toLowerCase(), path: req.path, data: req.body });
    }
    res.send('Success.');
});
function serve() {
    svr.listen(DRIVER_PORT || 3000, () => {
        console.log(`Server listening on port: ${DRIVER_PORT || 3000}`);
    });
}
exports.serve = serve;
